let socket = null;

window.addEventListener('load', function () {
    listenSocket();
    initiateFilters();
    cleanSearchBar();
});

const arrayHeading = ['Data Pack', 'Time', 'Id', 'Name', 'Dlc', 'Data'];
let arrayData = [];
let arrayCounter = 0;
let col = 6;
let subColData = 8;
let defaultFilter = null;
let filters = [];
let viewCounter = 0;
let showInHexa = false;

function initiateFilters() {
    for (let i = 0; i < col; i++) filters[i] = null;
}

function initiate() {
    arrayData = [];
    arrayCounter = 0;
    col = 6;
    defaultFilter = null;
    initiateFilters();
    viewCounter = 0;
}

function stopListenSocket() {
    if (socket != null) {
        socket.disconnect();
    }
}

function cleanSearchBar() {
    let search = document.getElementById('search');
    search.value = "";
}

function restart() {
    stopListenSocket();
    initiate();
    cleanSearchBar();
    supprTable();
    createTable();
    resetFilters();
    listenSocket();
}

function listenSocket() {

    socket = io();

    let dateFirstPacket = null;
    let firstPacket = true;

    socket.on('data', (counter, component) => {

        (document.getElementById('counterShow')).innerText = 'Number of data pack received : ' + counter;

        if (firstPacket) {
            firstPacket = false;
            dateFirstPacket = Date.now();
        }
        let time = '' + Date.now() - dateFirstPacket + ' ms';


        let data = [counter, time, component.id, component.name, component.dlc, component.data];
        arrayData[arrayCounter] = data;
        arrayCounter += 1;

        addTable(data);
    });
}


function addHeading() {
    let tableRow = document.createElement('tr');
    let itemCol = null;
    for (let i = 0; i < col; i++) {
        itemCol = document.createElement('th');
        itemCol.scope = "col";
        itemCol.innerText = arrayHeading[i];
        if (i == 5) {
            itemCol.colSpan = 8;
        } else {
            itemCol.rowSpan = 2;
        }
        tableRow.appendChild(itemCol);
    }
    let table = document.getElementById('table');

    table.appendChild(tableRow);
    tableRow = document.createElement('tr');
    for (let i = 1; i < 9; i++) {
        itemCol = document.createElement('th');
        itemCol.scope = "col";
        itemCol.innerText = "Byte " + i;
        tableRow.appendChild(itemCol);
    }
    table.appendChild(tableRow);
}

function addToTable(data) {
    let tableRow = document.createElement('tr');
    if (viewCounter % 2 === 0) {
        tableRow.style.backgroundColor = "rgba(133,156,255,0.5)";
    } else {
        tableRow.style.backgroundColor = "white";
    }

    let itemCol = null;
    for (let i = 0; i < col; i++) {
        if (i == 5) {
            for (let j = 0; j < subColData; j++) {
                let cell = document.createElement('td');
                if (data[i][j] == null) {
                    cell.innerText = '---';
                } else {
                    if (showInHexa) {
                        cell.innerText = data[i][j].toString(16).toUpperCase();
                    } else {
                        cell.innerText = data[i][j];
                    }
                }
                tableRow.appendChild(cell);
            }
        } else {
            itemCol = document.createElement('td');
            itemCol.innerText = data[i];
            tableRow.appendChild(itemCol);
        }

    }
    let table = document.getElementById('table');
    table.appendChild(tableRow);
    viewCounter++;
}

function changeToHexa() {
    showInHexa = !showInHexa;
    let menuItem = document.getElementById('hexaDeciButton');
    if (showInHexa) {
        menuItem.innerText = "ToDecimal";
    } else {
        menuItem.innerText = "ToHexadecimal";
    }
    reloadTable();
}

function filter(value) {
    let s = new String(value);
    s = s.toLowerCase();
    let index = s.indexOf("==");

    switch (s.substring(0, index)) {
        case "datapack":
            resetFilters();
            filters[0] = s.substring(index + 2);
            reloadTable();
            break;
        case "time":
            resetFilters();
            filters[1] = s.substring(index + 2);
            reloadTable();
            break;
        case "id":
            resetFilters();
            filters[2] = s.substring(index + 2);
            reloadTable();
            break;
        case "name":
            resetFilters();
            filters[3] = s.substring(index + 2);
            reloadTable();
            break;
        case "dlc":
            resetFilters();
            filters[4] = s.substring(index + 2);
            reloadTable();
            break;
        case "byte1":
        case "byte2":
        case "byte3":
        case "byte4":
        case "byte5":
        case "byte6":
        case "byte7":
        case "byte8":
            resetFilters();
            let nbByte = s.substring(4, 5);
            if (s.substring(index + 2, index + 4) == "0x") { // the user write in hexa
                filters[5] = { numByte: nbByte, search: s.substring(index + 4), searchBase: 'hexa' };
            } else { // the user search in deci
                if (showInHexa) filters[5] = { numByte: nbByte, search: s.substring(index + 2), searchBase: 'hexa' };

                else filters[5] = { numByte: nbByte, search: s.substring(index + 2), searchBase: 'deci' };
            }
            reloadTable();
            return;

        default:
            if (index == -1) {
                if (s.length == 0) {
                    resetFilters();
                    reloadTable();
                } else {
                    resetFilters();
                    defaultFilter = s;
                    reloadTable();
                }
                break;
            }
            for (let i = 0; i < col; i++) {
                if (filters[i] != null) {
                    filters[i] = null;
                    reloadTable();
                }

            }
            break;
    }
}

function resetFilters() {
    defaultFilter = null;
    for (let i = 0; i < col; i++) {
        filters[i] = null;
    }
}

function supprTable() {
    let tmp = document.getElementById("table");
    tmp.parentNode.removeChild(tmp);
}

function createTable() {
    let table = document.createElement("table");
    table.id = "table";
    document.getElementById("tableContainer").appendChild(table);
    addHeading();
}

function addTable(item) {
    let none = true;
    if (defaultFilter != null) {
        let filt = new String(defaultFilter);
        for (let i = 0; i < col; i++) {
            let s = new String(item[i]);
            if (s.indexOf(filt) != -1) {
                addToTable(item);
                return;
            }
        }
        return;
    }
    for (let i = 0; i < col; i++) {
        if (filters[i] != null) {
            none = false;
            if (i == 5) { // data filter
                let search = filters[5].search;
                let searchBase = filters[5].searchBase;
                let currentByte = item[5][filters[5].numByte - 1]; // always in decimal
                if (currentByte != null) {
                    if (searchBase === "hexa") {
                        if (currentByte.toString(16).toUpperCase() == search.toUpperCase()) {
                            addToTable(item);
                        }
                    } else if (searchBase === "deci") {
                        if (currentByte == search) {
                            addToTable(item);
                        }
                    }
                }
            } else {
                if (filters[i] == item[i]) addToTable(item);
            }

        }
    }
    if (none) addToTable(item);
}

function reloadTable() {
    supprTable();
    createTable();
    viewCounter = 0;
    arrayData.forEach((item) => {
        addTable(item);
    })
}