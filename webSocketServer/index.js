const express = require('express');
const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);

const componentsNames = ['engine', 'airbag', 'freezing system'];
const nbComponents = 3;

app.use(express.static(__dirname + "/public"));

io.on('connection', (socket) => {
    console.log('[Server] A user has been connected');
    let counter = 0;
    setInterval(() => {
        counter++;
        let message = simulateMessage();
        socket.emit('data', counter, message);
    }, 200);

    socket.on('disconnect', (reason) => {
        console.log(reason);
        switch (reason) {
            case 'client namespace disconnect': //socket.disconnect called
            case 'transport close': //the page has been close by the user for example
                console.log('[Server] A user has disconnected');
                break;
            default:
                console.log('[Server] A user has been disconnected');
                break;
        }
    });
});



http.listen(3000, () => {
    console.log('listening on : 3000');
});

function simulateMessage() {
    let dlc = Math.trunc(Math.random() * 8) + 1;
    let message = {
        idType: Math.trunc(Math.random() * 16), // prefix id on 4 bits [0-15]
        id: Math.trunc(Math.random() * (127)) + 1, // id of the node on 7 bits [1-127]
        name: componentsNames[Math.trunc(Math.random() * nbComponents)], // component name, we will know when we will have the documentation
        dlc: dlc, // Data Length Code, tell how many bytes of date there is [1-8]
        data: generateData(dlc)
    };
    return message;
}

// generate dlc bytes of data randomly
// return an array with a size of 8, each index are a byte of data, if no data on the index null;
function generateData(dlc) {
    let data = Array(8);
    for (let i = 0; i < dlc; i++) {
        data[i] = Math.trunc(Math.random() * 256); // 1 byte generated [0-255] (u_int8)
    }
    return data;
}