# Server Web for Raspberry [OUTDATED]

This version is currently outdated, if you want to have the final version check [STM32F7_CycloneServer repository](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_CycloneServer "STM32 repository link"). This repository containt the code of the version before merging with the real board : the STM32F7. The branches have not been merged.

## First Step

Download the dependencies in the package.json, in the webSocketServer folder :

```sh
npm install
```

After we can launch the server with :

```sh
node index.js
```

With a device connected in the same network of the raspberry, open a browser and in the URL write the IP adress of the raspberry in the local network and the port which is 3000, for my case :

```sh
192.168.43.243:3000
```

You are now connected to the server !

## Use Filter

You have a searchBar that allows you to filter the information.

<p align="center"><img alt="searchBar" src="/docs/searchBar.png"></p>

The following syntax is :
* datapack==X where X is the packet's number. 
* time==X ms where X is the time.
* id==X where X is the component's id.
* name==X where X is the component's name.
* dlc==X where X is a number in [ 1 ; 8 ] which represent the "Data Length Code" of a packet (dlc=X -> X byte of data) 
* byteX==Y where X is a number in [ 1 ; 8 ] which represents the byte that you want to filter and Y is the data : a number in [ 0 ; 255 ].
 
